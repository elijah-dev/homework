import { GET_MOVIES_IN_PROGRESS, GET_MOVIES_SUCCESS, GET_MOVIES_FAILURE } from '@/store/actions/get-movies.actions';
import { MovieModel } from '@/model/movie.types';
import { mapMoviesToModel } from '@/utils/map-functions.utils';
import { MoviesActionType } from '@/model/action.types';

interface MoviesState {
  inProgress: boolean;
  data: MovieModel[];
  error: Error | null;
}

export const initialState: MoviesState = {
  inProgress: false,
  data: [],
  error: null,
};

export function moviesReducer(state: MoviesState = initialState, action: MoviesActionType): MoviesState {
  switch (action.type) {
    case GET_MOVIES_IN_PROGRESS: {
      return {
        ...state,
        inProgress: true,
        error: null,
      };
    }
    case GET_MOVIES_SUCCESS: {
      const movieList = mapMoviesToModel(action.payload);
      return { ...state, inProgress: false, data: [...movieList] };
    }
    case GET_MOVIES_FAILURE: {
      return {
        ...state,
        inProgress: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
}
