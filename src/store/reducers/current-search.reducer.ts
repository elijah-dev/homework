import { SearchActionType } from '@/model/action.types';
import { SET_CURRENT_SEARCH_QUERY } from '@/store/actions/current-search.actions';

interface SearchState {
  query: string;
}

export const initialState = { query: '' };

export function currentSearchReducer(state: SearchState = initialState, action: SearchActionType): SearchState {
  switch (action.type) {
    case SET_CURRENT_SEARCH_QUERY:
      return {
        ...state,
        query: action.payload,
      };
    default:
      return state;
  }
}
