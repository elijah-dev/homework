import { GET_MOVIES_FAILURE, GET_MOVIES_IN_PROGRESS, GET_MOVIES_SUCCESS } from '@/store/actions/get-movies.actions';
import { moviesReducer, initialState } from '@/store/reducers/movies.reducer';
import { dummyData, dummyResponseData } from '@/utils/test-utils/dummy-data';

describe('moviesReducer', () => {
  const state = { ...initialState };
  it('should return the initial state', () => {
    const returnedState = moviesReducer(state, { type: '', payload: [], error: null });
    expect(returnedState).toEqual(state);
  });
  it('should handle GET_MOVIES_SUCCESS action', () => {
    const testPayload = [...dummyResponseData];
    const testData = [...dummyData];
    const returnedState = moviesReducer(state, {
      type: GET_MOVIES_SUCCESS,
      payload: testPayload,
      error: null,
    });
    expect(returnedState.data).toEqual(testData);
  });
  it('should handle GET_MOVIES_IN_PROGRESS action', () => {
    const returnedState = moviesReducer(state, {
      type: GET_MOVIES_IN_PROGRESS,
      payload: [],
      error: null,
    });
    expect(returnedState.inProgress).toBe(true);
  });
  it('should handle GET_MOVIES_FAILURE action', () => {
    const error = new Error('test');
    const returnedState = moviesReducer(state, {
      type: GET_MOVIES_FAILURE,
      payload: [],
      error,
    });
    expect(returnedState.error).toEqual(error);
  });
});
