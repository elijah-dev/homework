import { SET_CURRENT_SEARCH_QUERY } from '@/store/actions/current-search.actions';
import { initialState, currentSearchReducer } from '@/store/reducers/current-search.reducer';

describe('currentPageReducer', () => {
  const state = { ...initialState };
  it('should return the initial state', () => {
    const returnedState = currentSearchReducer(state, { type: '', payload: '' });
    expect(returnedState).toEqual(state);
  });
  it('should handle SET_CURRENT_PAGE action', () => {
    const returnedState = currentSearchReducer(state, { type: SET_CURRENT_SEARCH_QUERY, payload: 'test' });
    expect(returnedState.query).toMatch('test');
  });
});
