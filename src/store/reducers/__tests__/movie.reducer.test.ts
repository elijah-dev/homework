import { SET_SELECTED_MOVIE } from '@/store/actions/movie.actions';
import { initialState, movieReducer } from '@/store/reducers/movie.reducer';
import { dummyData } from '@/utils/test-utils/dummy-data';

describe('selectedMovieReducer', () => {
  const testPayload = { ...dummyData[0] };
  it('should return the initial state', () => {
    const state = { ...initialState };
    const returnedState = movieReducer(state, { type: '', payload: testPayload });
    expect(returnedState).toEqual(state);
  });
  it('should handle SET_SELECTED_MOVIE action', () => {
    const state = { ...initialState };
    const returnedState = movieReducer(state, { type: SET_SELECTED_MOVIE, payload: testPayload });
    expect(returnedState.data).toEqual(testPayload);
  });
});
