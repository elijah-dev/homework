import { MovieActionType } from '@/model/action.types';
import { MovieModel } from '@/model/movie.types';
import {
  SET_SELECTED_MOVIE,
  GET_MOVIE_FAILURE,
  GET_MOVIE_IN_PROGRESS,
  GET_MOVIE_NOT_FOUND,
} from '@/store/actions/movie.actions';
import { Reducer } from 'redux';

type SelectedMovieState = {
  inProgress: boolean;
  data: MovieModel;
  notFound: boolean,
  error?: null | Error;
};

export const initialState = {
  inProgress: false,
  data: {
    id: -1,
    title: '',
    tagline: '',
    voteAverage: 0,
    voteCount: 0,
    releaseDate: '',
    posterPath: '',
    overview: '',
    budget: 0,
    revenue: 0,
    genres: [],
    runtime: 0,
  },
  notFound: false,
  error: null,
};

export const movieReducer: Reducer<SelectedMovieState, MovieActionType> = (
  state = initialState,
  action,
): SelectedMovieState => {
  switch (action.type) {
    case SET_SELECTED_MOVIE: {
      return {
        ...state,
        inProgress: false,
        notFound: false,
        data: action.payload ? action.payload : initialState.data,
      };
    }
    case GET_MOVIE_IN_PROGRESS: {
      return {
        ...state,
        inProgress: true,
      };
    }
    case GET_MOVIE_NOT_FOUND: {
      return {
        ...state,
        inProgress: false,
        notFound: true,
        data: initialState.data,
      };
    }
    case GET_MOVIE_FAILURE: {
      return {
        ...state,
        inProgress: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
};
