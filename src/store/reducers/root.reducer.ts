import { combineReducers } from 'redux';
import { moviesReducer } from '@/store/reducers/movies.reducer';
import { currentSearchReducer } from '@/store/reducers/current-search.reducer';
import { movieReducer } from '@/store/reducers/movie.reducer';

export const rootReducer = combineReducers({
  movies: moviesReducer,
  selectedMovie: movieReducer,
  currentSearch: currentSearchReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
