import { createStore, applyMiddleware, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer, RootState } from '@/store/reducers/root.reducer';
import thunk from 'redux-thunk';

const middleware = [thunk];

export function configureStore(initialState?: RootState): Store {
  const store: Store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));
  return store;
}
