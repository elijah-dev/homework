import { MovieResponseModel } from '@/model/movie.types';
import { MoviesActionType } from '@/model/action.types';
import { CustomThunkAction } from '@/model/thunk.types';

export const GET_MOVIES_IN_PROGRESS = 'GET_MOVIES_IN_PROGRESS';
export const GET_MOVIES_SUCCESS = 'GET_MOVIES_SUCCESS';
export const GET_MOVIES_FAILURE = 'GET_MOVIES_FAILURE';

export function getMoviesInProgress(): MoviesActionType {
  return { type: GET_MOVIES_IN_PROGRESS, payload: [], error: null };
}

export function getMoviesSuccess(movies: MovieResponseModel[]): MoviesActionType {
  return { type: GET_MOVIES_SUCCESS, payload: movies, error: null };
}

export function getMoviesFailure(error: Error): MoviesActionType {
  return { type: GET_MOVIES_FAILURE, payload: [], error };
}

export function getMovies(
  baseUrl: string,
  queryString: string,
): CustomThunkAction {
  return (dispatch) => {
    dispatch(getMoviesInProgress());
    return fetch(`${baseUrl}${queryString}`)
      .then((res) => res.json())
      .then(({ data }) => dispatch(getMoviesSuccess(data)))
      .catch((error) => dispatch(getMoviesFailure(error)));
  };
}
