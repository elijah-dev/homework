import { SET_CURRENT_SEARCH_QUERY, setCurrentSearchQuery } from '@/store/actions/current-search.actions';

describe('currentPageActions', () => {
  it('setCurrentPage returns expected action', () => {
    const expectedAction = {
      type: SET_CURRENT_SEARCH_QUERY,
      payload: 'test',
    };
    expect(setCurrentSearchQuery('test')).toEqual(expectedAction);
  });
});
