import { SET_SELECTED_MOVIE, setSelectedMovie } from '@/store/actions/movie.actions';
import { dummyData } from '@/utils/test-utils/dummy-data';

describe('Selected movie actions', () => {
  it('setSelectedMovie should return correct action', () => {
    const testPayload = { ...dummyData[0] };
    const expectedAction = {
      type: SET_SELECTED_MOVIE,
      payload: testPayload,
    };
    expect(setSelectedMovie(testPayload)).toEqual(expectedAction);
  });
});
