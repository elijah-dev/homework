import {
  GET_MOVIES_FAILURE,
  GET_MOVIES_IN_PROGRESS,
  GET_MOVIES_SUCCESS,
  getMovies,
  getMoviesFailure,
  getMoviesInProgress,
  getMoviesSuccess,
} from '@/store/actions/get-movies.actions';
import { RootState } from '@/store/reducers/root.reducer';
import { dummyResponseData } from '@/utils/test-utils/dummy-data';
import { mockStore } from '@/utils/test-utils/mock-store';
import fetchMock from 'fetch-mock';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

describe('getMovies actions', () => {
  afterEach(() => {
    fetchMock.reset();
  });
  it('getMoviesInProgress returns expected action', () => {
    const expectedAction = {
      type: GET_MOVIES_IN_PROGRESS,
      payload: [],
      error: null,
    };
    expect(getMoviesInProgress()).toEqual(expectedAction);
  });
  it('getMoviesSuccess returns expected action', () => {
    const expectedAction = {
      type: GET_MOVIES_SUCCESS,
      payload: [...dummyResponseData],
      error: null,
    };
    expect(getMoviesSuccess([...dummyResponseData])).toEqual(expectedAction);
  });
  it('getMoviesFailure returns expected action', () => {
    const error = new Error('test');
    const expectedAction = {
      type: GET_MOVIES_FAILURE,
      payload: [],
      error,
    };
    expect(getMoviesFailure(error)).toEqual(expectedAction);
  });
  it('getMovies dispatches correct actions', () => {
    const movies = [...dummyResponseData];
    const url = 'http://test.com';
    fetchMock.getOnce(url, {
      body: { data: movies },
      headers: { 'content-type': 'application/json' },
    });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_MOVIES_IN_PROGRESS,
        payload: [],
        error: null,
      },
      {
        type: GET_MOVIES_SUCCESS,
        payload: movies,
        error: null,
      },
    ];
    const dispatch = store.dispatch as ThunkDispatch<RootState, null, AnyAction>;
    return dispatch(getMovies(url, '')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('getMovies dispatches getMoviesFailure on error', () => {
    const url = 'http://test.com';
    const testError = new Error('test');
    fetchMock.getOnce(url, { throws: testError, headers: { 'content-type': 'application/json' } });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_MOVIES_IN_PROGRESS,
        payload: [],
        error: null,
      },
      {
        type: GET_MOVIES_FAILURE,
        payload: [],
        error: testError,
      },
    ];
    const dispatch = store.dispatch as ThunkDispatch<RootState, null, AnyAction>;
    return dispatch(getMovies(url, '')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
