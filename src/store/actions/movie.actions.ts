import { MovieActionType } from '@/model/action.types';
import { MovieModel } from '@/model/movie.types';
import { castToMovieObject } from '@/utils/map-functions.utils';
import { CustomThunkAction } from '@/model/thunk.types';

export const SET_SELECTED_MOVIE = 'SET_SELECTED_MOVIE';
export const GET_MOVIE_IN_PROGRESS = 'GET_MOVIE_IN_PROGRESS';
export const GET_MOVIE_NOT_FOUND = 'GET_MOVIE_NOT_FOUND';
export const GET_MOVIE_FAILURE = 'GET_MOVIE_FAILURE';

export const setSelectedMovie = (data: MovieModel): MovieActionType => ({
  type: SET_SELECTED_MOVIE,
  payload: data,
});

export const getMovieInProgress = (): MovieActionType => ({ type: GET_MOVIE_IN_PROGRESS });

export const getMovieNotFound = (): MovieActionType => ({ type: GET_MOVIE_NOT_FOUND });

export const getMovieFailure = (error: Error): MovieActionType => ({
  type: GET_MOVIE_FAILURE,
  error,
});

export function getMovieById(
  baseUrl: string,
  id: string,
): CustomThunkAction {
  return (dispatch) => {
    dispatch(getMovieInProgress());
    return fetch(`${baseUrl}/${id}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.id) {
          dispatch(setSelectedMovie(castToMovieObject(data)));
        } else dispatch(getMovieNotFound());
      })
      .catch((error) => dispatch(getMovieFailure(error)));
  };
}
