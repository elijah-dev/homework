import { SearchActionType } from '@/model/action.types';

export const SET_CURRENT_SEARCH_QUERY = 'SET_CURRENT_SEARCH_QUERY';

export const setCurrentSearchQuery = (page: string): SearchActionType => ({
  type: SET_CURRENT_SEARCH_QUERY,
  payload: page,
});
