import React from 'react';
import { renderToString } from 'react-dom/server';
import { App } from '@/app.component';
import { Request, Response } from 'express';
import { matchPath, StaticRouter } from 'react-router-dom';
import { ServerStyleSheet } from 'styled-components';
import { configureStore } from '@/store/store';
import { RootState } from '@/store/reducers/root.reducer';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { routes } from '@/config/routes';
import { Template } from './template';

export default function serverRenderer() {
  return (req: Request, res: Response): void => {
    const sheet = new ServerStyleSheet();
    const store = configureStore();
    const dispatch = store.dispatch as ThunkDispatch<RootState, null, AnyAction>;

    const Root = () => (
      <StaticRouter context={{}} location={req.url}>
        <App store={store} />
      </StaticRouter>
    );

    function finalRender() {
      renderToString(sheet.collectStyles(<Root />));
      const styleTags = sheet.getStyleElement();
      sheet.seal();
      const markup = renderToString(
        <Template state={store.getState()} styles={styleTags}>
          <Root />
        </Template>,
      );
      res.send(markup);
    }

    const currentRoute = routes.find((route) => matchPath(req.path, route));
    if (currentRoute && currentRoute.fetchData) {
      currentRoute.fetchData(dispatch, req.url).then(() => finalRender());
    } else finalRender();
  };
}
