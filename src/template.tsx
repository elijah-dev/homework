import React, { ReactNode } from 'react';
import { RootState } from './store/reducers/root.reducer';

interface TemplateProps {
  state: RootState;
  styles: ReactNode;
  children: ReactNode;
}

export const Template = ({ state, styles, children }: TemplateProps): JSX.Element => (
  <html lang="en-US">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link href="/main.css" rel="stylesheet" type="text/css" />
      {styles}
      <title>Movie Finder</title>
    </head>
    <body>
      <div id="root">{children}</div>
      <script
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: `window.PRELOADED_STATE = ${JSON.stringify(state)}` }}
      />
      <script src="/main.js" />
    </body>
  </html>
);
