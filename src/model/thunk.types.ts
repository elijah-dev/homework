import { RootState } from '@/store/reducers/root.reducer';
import { Action, AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { MovieActionType, MoviesActionType } from './action.types';

export type CustomThunkDispatch = ThunkDispatch<RootState, null, AnyAction>;

export type CustomPromise = Promise<MoviesActionType | MovieActionType | void>;

export type CustomThunkAction = ThunkAction<CustomPromise, RootState, null, Action>;
