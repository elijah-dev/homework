import { MovieModel, MovieResponseModel } from '@/model/movie.types';

export interface MovieActionType {
  type: string;
  payload?: MovieModel;
  error?: Error | null;
}

export interface SearchActionType {
  type: string;
  payload: string;
}

export interface QueryParamsActionType {
  type: string;
  payload: string;
}

export interface MoviesActionType {
  type: string;
  payload: MovieResponseModel[];
  error: Error | null;
}
