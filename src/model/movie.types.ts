export interface MovieResponseModel {
  id: number,
  title: string,
  tagline: string,
  // eslint-disable-next-line camelcase
  vote_average: number,
  // eslint-disable-next-line camelcase
  vote_count: number,
  // eslint-disable-next-line camelcase
  release_date: string,
  // eslint-disable-next-line camelcase
  poster_path: string | null,
  overview: string,
  budget: number,
  revenue: number,
  genres: string[],
  runtime: number | null,
}

export interface MovieModel {
  id: number;
  title: string;
  tagline: string;
  voteAverage: number;
  voteCount: number;
  releaseDate: string;
  posterPath: string;
  overview: string;
  budget: number;
  revenue: number;
  genres: string[];
  runtime: number;
}
