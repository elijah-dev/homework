export enum Path {
  Search = '/search',
  Movie = '/movies',
  MovieId = '/movies/:id'
}

export enum SearchBy {
  Title = 'title',
  Genres = 'genres',
}
export enum SortBy {
  Date = 'release_date',
  Rating = 'vote_average',
}
