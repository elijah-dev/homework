import { Path } from '@/model/enums';
import { setCurrentSearchQuery } from '@/store/actions/current-search.actions';
import { getMovies } from '@/store/actions/get-movies.actions';
import { API_URL } from '@/config/constants';
import { CustomPromise, CustomThunkDispatch } from '@/model/thunk.types';
import { getMovieById } from '@/store/actions/movie.actions';

export const routes = [
  {
    path: Path.Search,
    fetchData: (dispatch: CustomThunkDispatch, url: string): CustomPromise => {
      const searchQuery = url.replace(/\/search/, '');
      dispatch(setCurrentSearchQuery(searchQuery));
      return dispatch(getMovies(API_URL, searchQuery));
    },
  },
  {
    path: Path.MovieId,
    fetchData: (dispatch: CustomThunkDispatch, url: string): CustomPromise => {
      const results = url.match(/\d+/);
      const id = results ? results[0] : '';
      return dispatch(getMovieById(API_URL, id));
    },
  },
];
