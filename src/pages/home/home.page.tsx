import React, { memo } from 'react';

function HomePageComponent() {
  return (
    <div>
      <h1>Welcome to MovieFinder!</h1>
      <h2>Use search bar on top to find some movies to watch</h2>
    </div>
  );
}

export const HomePage = memo(HomePageComponent);
