import React from 'react';
import { HomePage } from '@/pages/home/home.page';
import { getMockState, mockStore } from '@/utils/test-utils/mock-store';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';

describe('<HomePage />', () => {
  it('should match snapshot', () => {
    const state = getMockState();
    const store = mockStore(state);
    const wrapper = renderWithProviders(<HomePage />, store);
    expect(wrapper).toMatchSnapshot();
  });
});
