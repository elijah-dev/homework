import React, { memo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MovieModel } from '@/model/movie.types';
import { CircularSpinner } from '@common/circular-spinner/circular-spinner.component';
import { StyledMovieList } from '@/pages/movie-list/movie-list.style';
import { MovieListTopBar } from '@/components/movie-list-top-bar/movie-list-top-bar.component';
import { MovieCard } from '@/components/movie-card/movie-card.component';
import { RootState } from '@/store/reducers/root.reducer';
import { useLocation } from 'react-router-dom';
import { getMovies } from '@/store/actions/get-movies.actions';
import { setCurrentSearchQuery } from '@/store/actions/current-search.actions';
import { API_URL } from '@/config/constants';

function MovieListComponent() {
  const dispatch = useDispatch();
  const query = useLocation().search;
  const movieList = useSelector((state: RootState) => state.movies.data);
  const isLoading = useSelector(({ movies }: RootState) => movies.inProgress);
  const currentSearch = useSelector(({ currentSearch }: RootState) => currentSearch.query);

  useEffect(() => {
    if (currentSearch !== query) {
      dispatch(getMovies(API_URL, query));
      dispatch(setCurrentSearchQuery(query));
    }
  }, [dispatch, query, currentSearch]);

  return (
    <CircularSpinner isLoading={isLoading}>
      {movieList.length === 0 ? (
        <h2>No movies found</h2>
      ) : (
        <StyledMovieList>
          <MovieListTopBar moviesCount={movieList.length} />
          {movieList.map((movie: MovieModel) => (
            <MovieCard key={movie.id} movie={movie} />
          ))}
        </StyledMovieList>
      )}
    </CircularSpinner>
  );
}

export const MovieList = memo(MovieListComponent);
