import React from 'react';
import { MovieList } from '@/pages/movie-list/movie-list.page';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { dummyData } from '@/utils/test-utils/dummy-data';
import { getMockState, mockStore } from '@/utils/test-utils/mock-store';
import { MovieCard } from '@/components/movie-card/movie-card.component';
import { CircularSpinner } from '@/components/common/circular-spinner/circular-spinner.component';

describe('<MovieList />', () => {
  it('renders correctly', () => {
    const initialState = getMockState();
    const store = mockStore({ ...initialState });
    const wrapper = renderWithProviders(<MovieList />, store);
    expect(wrapper).toMatchSnapshot();
  });
  it('should render placeholder text if no movie data is in store', () => {
    const initialState = getMockState();
    const store = mockStore({ ...initialState });
    const wrapper = renderWithProviders(<MovieList />, store);
    expect(wrapper.text()).toMatch(/no movies found/i);
  });
  it('should render cards if store has movies data', () => {
    const initialState = getMockState();
    const store = mockStore({ ...initialState, movies: { data: dummyData } });
    const wrapper = renderWithProviders(<MovieList />, store);
    expect(wrapper.exists(MovieCard)).toBe(true);
  });
  it('should render spinner if movie data fetching is in progress', () => {
    const initialState = getMockState();
    const store = mockStore({ ...initialState, movies: { data: [], inProgress: true } });
    const wrapper = renderWithProviders(<MovieList />, store);
    expect(wrapper.exists(CircularSpinner)).toBe(true);
  });
});
