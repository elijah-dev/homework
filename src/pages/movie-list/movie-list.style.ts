import styled from 'styled-components';

const topBarHeight = '40px';

export const StyledMovieList = styled.div`
  padding: 20px;
  padding-top: ${topBarHeight};
  position: relative;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;

  & * {
    color: ${(props) => props.theme.colors.primary[200]};
  }

  & > div:first-child {
    height: ${topBarHeight};
    position: fixed;
    top: ${(props) => props.theme.headerHeight};
    z-index: 1;
    background-color: ${(props) => props.theme.colors.primary[500]};
    width: 100%;
    padding: 10px 15px;
    display: flex;
    justify-content: space-between;
  }

  & > span:first-child {
    font-weight: 600;
  }
`;
