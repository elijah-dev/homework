import React, { memo } from 'react';

function FourOFourPageComponent() {
  return (
    <div>
      <h1>This page does not exist</h1>
    </div>
  );
}

export const FourOFourPage = memo(FourOFourPageComponent);
