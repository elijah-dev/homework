import React, { memo, useEffect } from 'react';
import { ImageWithFallback } from '@common/image-with-fallback/image-with-fallback.component';
import { MovieInfoBlock } from '@/components/movie-info-block/movie-info-block.component';
import { CloseButton } from '@/components/close-button/close-button.component';
import { StyledMoviePage } from '@/pages/movie-page/movie.style';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/store/reducers/root.reducer';
import { useParams } from 'react-router-dom';
import { getMovieById } from '@/store/actions/movie.actions';
import { API_URL } from '@/config/constants';
import { CircularSpinner } from '@/components/common/circular-spinner/circular-spinner.component';

function MoviePageComponent() {
  const dispatch = useDispatch();
  const { data: movie, inProgress, notFound } = useSelector(({ selectedMovie }: RootState) => selectedMovie);
  const { id }: { id: string } = useParams();

  useEffect(() => {
    if (movie.id !== parseInt(id, 10)) {
      dispatch(getMovieById(API_URL, id));
    }
  }, [movie.id, id, dispatch]);

  return (
    <CircularSpinner isLoading={inProgress}>
      <StyledMoviePage>
        {movie.id < 0 && notFound ? (
          <h1>Movie not found</h1>
        ) : (
          <>
            <div>
              <ImageWithFallback alt={movie.title} src={movie.posterPath} fitHeight />
            </div>
            <MovieInfoBlock movie={movie} />
          </>
        )}
        <CloseButton />
      </StyledMoviePage>
    </CircularSpinner>
  );
}

export const MoviePage = memo(MoviePageComponent);
