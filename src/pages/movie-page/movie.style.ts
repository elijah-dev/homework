import styled from 'styled-components';

export const StyledMoviePage = styled.div`
  position: relative;
  background-color: ${(props) => props.theme.colors.primary[500]};
  min-height: 500px;
  width: 80%;
  display: flex;
  padding: 20px;
  animation: fadein 400ms linear forwards;

  @keyframes fadein {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }

  & * {
    color: ${(props) => props.theme.colors.primary[200]};
  }

  & h1 {
    margin: auto;
  }

  & div:first-child {
    flex-shrink: 0;
    height: 500px;
    min-width: 300px;
    background-color: ${(props) => props.theme.colors.primary[400]};
    margin-right: 20px;
  }

  & h2 {
    text-transform: uppercase;
    color: ${(props) => props.theme.colors.primary[200]}
  }
`;
