import React from 'react';
import { ReactWrapper } from 'enzyme';
import { MoviePage } from '@/pages/movie-page/movie.page';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { getMockState, mockStore } from '@/utils/test-utils/mock-store';
import { Route } from 'react-router-dom';

describe('<MoviePage />', () => {
  let wrapper: ReactWrapper;
  const mockState = getMockState();
  const store = mockStore(mockState);
  beforeEach(() => {
    wrapper = renderWithProviders(
      <Route path="/movie/-1">
        <MoviePage />
      </Route>,
      store,
    );
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
