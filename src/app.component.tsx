import React, { memo } from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from '@/app.theme';
import { GlobalErrorBoundary } from '@common/global-error-boundary/global-error-boundary.component';
import '@/global.css';
import '@/assets/img/favicon.ico';
import { HomePage } from '@/pages/home/home.page';
import { Route, Switch } from 'react-router-dom';
import { Layout } from '@/components/layout/layout.component';
import { MovieList } from '@/pages/movie-list/movie-list.page';
import { MoviePage } from '@/pages/movie-page/movie.page';
import { FourOFourPage } from '@/pages/404/404.page';
import { Path } from '@/model/enums';
import { Store } from 'redux';
import { Provider } from 'react-redux';

function AppComponent({ store }: { store: Store }) {
  return (
    <React.StrictMode>
      <GlobalErrorBoundary>
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <Layout>
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path={Path.Search} component={MovieList} />
                <Route path={`${Path.MovieId}`} component={MoviePage} />
                <Route path={Path.Movie} component={HomePage} />
                <Route path="*" component={FourOFourPage} />
              </Switch>
            </Layout>
          </ThemeProvider>
        </Provider>
      </GlobalErrorBoundary>
    </React.StrictMode>
  );
}

export const App = memo(AppComponent);
