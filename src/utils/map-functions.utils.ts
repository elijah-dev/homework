import { MovieModel, MovieResponseModel } from '@/model/movie.types';

export function castToMovieObject(movieObj: MovieResponseModel): MovieModel {
  return {
    id: movieObj.id,
    title: movieObj.title,
    tagline: movieObj.tagline,
    voteAverage: movieObj.vote_average,
    voteCount: movieObj.vote_count,
    releaseDate: movieObj.release_date,
    posterPath: movieObj.poster_path || '',
    overview: movieObj.overview,
    budget: movieObj.budget,
    revenue: movieObj.revenue,
    genres: movieObj.genres,
    runtime: movieObj.runtime || 0,
  };
}

export function mapMoviesToModel(movieList: MovieResponseModel[]): MovieModel[] {
  return movieList.map((movie: MovieResponseModel) => castToMovieObject(movie));
}
