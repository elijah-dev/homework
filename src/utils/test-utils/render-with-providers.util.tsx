import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { ThemeProvider } from 'styled-components';
import { theme } from '@/app.theme';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { MemoryRouter } from 'react-router-dom';

export function renderWithProviders(tree: React.ReactElement, store: Store, options = {}): ReactWrapper {
  const providerWrapper = (props: { children: React.ReactNode }) => (
    <MemoryRouter initialEntries={['/', '/movies/-1']} initialIndex={1}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
      </Provider>
    </MemoryRouter>
  );
  return mount(tree, { wrappingComponent: providerWrapper, ...options });
}
