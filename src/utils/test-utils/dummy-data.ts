export const dummyData = [
  {
    id: 1,
    title: 'movie one',
    tagline: 'tagline',
    voteAverage: 4,
    voteCount: 1,
    releaseDate: '2010-01-01',
    posterPath: 'path',
    overview: 'overview',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
  {
    id: 2,
    title: 'movie two',
    tagline: 'tagline',
    voteAverage: 2,
    voteCount: 1,
    releaseDate: '2020-01-01',
    posterPath: '',
    overview: '',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
  {
    id: 3,
    title: 'movie three',
    tagline: 'tagline',
    voteAverage: 8,
    voteCount: 1,
    releaseDate: '2015-01-01',
    posterPath: 'path',
    overview: 'overview',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
];

export const dummyResponseData = [
  {
    id: 1,
    title: 'movie one',
    tagline: 'tagline',
    vote_average: 4,
    vote_count: 1,
    release_date: '2010-01-01',
    poster_path: 'path',
    overview: 'overview',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
  {
    id: 2,
    title: 'movie two',
    tagline: 'tagline',
    vote_average: 2,
    vote_count: 1,
    release_date: '2020-01-01',
    poster_path: '',
    overview: '',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
  {
    id: 3,
    title: 'movie three',
    tagline: 'tagline',
    vote_average: 8,
    vote_count: 1,
    release_date: '2015-01-01',
    poster_path: 'path',
    overview: 'overview',
    budget: 1,
    revenue: 1,
    genres: ['genre'],
    runtime: 1,
  },
];
