import { configureStore } from '@/store/store';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

export const mockStore = configureMockStore([thunk]);

const store = configureStore();

type mockState = ReturnType<typeof store.getState>;

export function getMockState(): mockState {
  return JSON.parse(JSON.stringify(store.getState()));
}
