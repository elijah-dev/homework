import React from 'react';
import { shallow } from 'enzyme';

const Example = () => <div />;

describe('<Example />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<Example />);
    expect(wrapper).toMatchSnapshot();
  });
});
