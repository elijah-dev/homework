export function formatCurrency(value: number): string {
  return new Intl.NumberFormat('en', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
  }).format(value);
}

export function formatRating(rating: number): string {
  return `\u2605 ${rating}`;
}

export function formatDate(dateString: string): string {
  try {
    const date = new Date(dateString);
    const formattedDate = new Intl.DateTimeFormat('en', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    }).format(date);
    return formattedDate;
  } catch {
    return 'N/A';
  }
}
