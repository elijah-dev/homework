import { SearchBy } from '@/model/enums';

export function makeQuery(searchOption: string, searchValue: string, sortMethod: string): string {
  const searchBy = `searchBy=${searchOption}`;
  let searchQuery = '';

  switch (searchOption) {
    case SearchBy.Title:
      searchQuery = `search=${searchValue.trim()}`;
      break;
    case SearchBy.Genres:
      searchQuery = `filter=${searchValue.trim().replace(/ /g, ',')}`;
      break;
    default:
      searchQuery = `search=${searchValue.trim()}`;
      break;
  }

  const queryString = `?${searchBy}&${searchQuery}&sortBy=${sortMethod}&sortOrder=desc&limit=50`;
  return queryString;
}
