export const theme = {
  fonts: { primary: "'Montserrat', sans-serif" },
  colors: {
    primary: {
      600: '#000000',
      500: '#303030',
      400: '#5E5E5E',
      300: '#919191',
      200: '#C6C6C6',
      100: '#FFFFFF',
    },
    secondary: {
      500: '#760000',
      400: '#960000',
      300: '#B80000',
      200: '#DB0000',
      100: '#FF0000',
    },
  },
  headerHeight: '90px',
};
