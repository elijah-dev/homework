import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '@/app.component';
import { BrowserRouter } from 'react-router-dom';
import { configureStore } from '@/store/store';
import { RootState } from '@/store/reducers/root.reducer';

interface WindowWithState extends Window {
  PRELOADED_STATE?: RootState;
}

const windowWithState: WindowWithState = window;

const store = configureStore(windowWithState.PRELOADED_STATE);

ReactDOM.hydrate(
  <BrowserRouter>
    <App store={store} />
  </BrowserRouter>,
  document.getElementById('root'),
);
