import React from 'react';
import { MovieInfoBlock } from '@/components/movie-info-block/movie-info-block.component';
import { dummyData } from '@/utils/test-utils/dummy-data';
import { shallow } from 'enzyme';

describe('<MovieInfoBlock />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<MovieInfoBlock movie={dummyData[0]} />);
    expect(wrapper).toMatchSnapshot();
  });
  it('renders date correctly with valid date string', () => {
    const newData = {
      ...dummyData[0],
      releaseDate: '2020-10-10',
    };
    const wrapper = shallow(<MovieInfoBlock movie={newData} />);
    expect(wrapper.text()).toMatch('October 10, 2020');
  });
  it('renders date correctly with invalid date string', () => {
    const newData = {
      ...dummyData[0],
      releaseDate: 'invalid string',
    };
    const wrapper = shallow(<MovieInfoBlock movie={newData} />);
    expect(wrapper.text()).toMatch('N/A');
  });
  it('does not render rating if value is zero', () => {
    const newData = {
      ...dummyData[0],
      voteAverage: 0,
    };
    const wrapper = shallow(<MovieInfoBlock movie={newData} />);
    expect(wrapper.find('.misc-info').find('span').length).toBe(2);
  });
  it('does not render budget if value is zero', () => {
    const newData = {
      ...dummyData[0],
      budget: 0,
    };
    const wrapper = shallow(<MovieInfoBlock movie={newData} />);
    expect(wrapper.find('.misc-info').find('span').length).toBe(2);
  });
  it('does not render runtime if value is zero', () => {
    const newData = {
      ...dummyData[0],
      runtime: 0,
    };
    const wrapper = shallow(<MovieInfoBlock movie={newData} />);
    expect(wrapper.find('.misc-info').find('span').length).toBe(2);
  });
});
