import styled from 'styled-components';

export const StyledMovieInfoBlock = styled.div`
  position: relative;

  & h2 {
    text-transform: uppercase;
    font-size: 2.2rem;
    line-height: 2rem;
  }

  & h4 {
    font-weight: 400;
    font-style: italic;
    color: ${(props) => props.theme.colors.primary[300]};
    margin-bottom: 20px;
  }

  & .release-date {
    margin-bottom: 20px;
  }

  & .misc-info {
    border-top: 1px solid;
    border-bottom: 1px solid;
    margin-bottom: 20px;
    padding-top: 6px;
    display: flex;
    justify-content: space-between;
  }

  & .genres {
    color: ${(props) => props.theme.colors.primary[300]};
    margin-bottom: 10px;
  }

  & .overview {
    text-align: justify;
    font-size: 1.1rem;
    margin-bottom: 40px;
  }
`;
