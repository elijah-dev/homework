import React, { memo } from 'react';
import { formatCurrency, formatDate, formatRating } from '@/utils/format-functions.util';
import { MovieModel } from '@/model/movie.types';
import { StyledMovieInfoBlock } from '@/components/movie-info-block/movie-info-block.style';

interface MovieInfoBlockProps {
  movie: MovieModel;
}

function MovieInfoBlockComponent(
  { movie: { title, tagline, voteAverage, releaseDate, overview, budget, genres, runtime } }: MovieInfoBlockProps,
) {
  return (
    <StyledMovieInfoBlock>
      <h2>{title}</h2>
      <h4>{tagline}</h4>
      <p className="release-date">{releaseDate && formatDate(releaseDate)}</p>
      <p className="misc-info">
        {voteAverage > 0 && <span>{formatRating(voteAverage)}</span>}
        {budget > 0 && <span>{formatCurrency(budget)}</span>}
        {runtime > 0 && <span>{`${runtime} min`}</span>}
      </p>
      <p className="genres">{genres && genres.join(', ')}</p>
      <p className="overview">{overview}</p>
    </StyledMovieInfoBlock>
  );
}

export const MovieInfoBlock = memo(MovieInfoBlockComponent);
