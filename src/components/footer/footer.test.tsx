import React from 'react';
import 'jest-styled-components';
import { Footer } from '@/components/footer/footer.component';
import { shallow } from 'enzyme';

describe('<Footer />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<Footer />);
    expect(wrapper).toMatchSnapshot();
  });
});
