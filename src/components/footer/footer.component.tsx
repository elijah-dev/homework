import React, { memo } from 'react';
import { StyledFooter } from '@/components/footer/footer.style';

function FooterComponent() {
  return (
    <StyledFooter>
      <span>movie</span>
      <span>finder</span>
      <span>© 2020</span>
    </StyledFooter>
  );
}

export const Footer = memo(FooterComponent);
