import styled from 'styled-components';

export const StyledFooter = styled.footer`
  height: 40px;
  flex-shrink: 0;
  background-color: ${(props) => props.theme.colors.primary[600]};
  font-family: ${(props) => props.theme.fonts.primary};
  color: ${(props) => props.theme.colors.primary[100]};
  text-transform: uppercase;
  font-size: 0.9rem;
  display: flex;
  align-items: center;
  justify-content: center;

  & span:nth-child(2) {
    color: ${(props) => props.theme.colors.secondary[400]};
  }
`;
