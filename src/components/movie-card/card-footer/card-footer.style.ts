import styled from 'styled-components';

export const StyledCardFooter = styled.div`
  position: absolute;
  bottom: 0;
  background-color: ${(props) => props.theme.colors.primary[500]}E6;
  width: 280px;
  height: 100px;
  z-index: 1;
  padding: 8px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  & h4 {
    font-size: 0.9rem;
    font-weight: 600;
    text-transform: uppercase;
  }

  & h4 + p {
    font-size: 0.8rem;
  }

  & p + p {
    display: flex;
    justify-content: space-between;
  }
`;
