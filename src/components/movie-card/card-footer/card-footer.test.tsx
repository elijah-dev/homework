import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { CardFooter } from '@/components/movie-card/card-footer/card-footer.component';

describe('<CardFooter />', () => {
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(<CardFooter title="movie" voteAverage={10} releaseDate="2020-01-01" genres={['genres']} />);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('does not render release year if string is invalid date', () => {
    wrapper.setProps({ releaseDate: 'invalid_date' });
    expect(wrapper.find('span').at(1).text()).toBeFalsy();
  });
});
