import React, { memo } from 'react';
import { formatRating } from '@/utils/format-functions.util';
import { StyledCardFooter } from '@/components/movie-card/card-footer/card-footer.style';

interface CardFooterProps {
  title: string;
  releaseDate: string;
  voteAverage: number;
  genres: string[];
}

function CardFooterComponent({ title, releaseDate, voteAverage, genres }: CardFooterProps) {
  return (
    <StyledCardFooter>
      <h4>{title}</h4>
      <p>{genres.join(', ')}</p>
      <p>
        <span>{voteAverage > 0 && formatRating(voteAverage)}</span>
        <span>{releaseDate && (new Date(releaseDate).getFullYear() || '')}</span>
      </p>
    </StyledCardFooter>
  );
}

export const CardFooter = memo(CardFooterComponent);
