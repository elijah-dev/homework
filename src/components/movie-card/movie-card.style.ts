import styled from 'styled-components';

export const StyledMovieCard = styled.div`
  position: relative;
  width: 280px;
  height: 420px;
  border-radius: 10px;
  transition-duration: 200ms;
  margin: 10px;
  background-color: ${(props) => props.theme.colors.primary[300]};
  animation: fadein 400ms linear forwards;

  @keyframes fadein {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }

  &:hover {
    cursor: pointer;
    transform: scale(1.02);
  }

  & .img-fallback-text {
    text-align: center;
    margin-top: 50%;
  }
`;
