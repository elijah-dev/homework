import React from 'react';
import { ReactWrapper } from 'enzyme';
import { MovieCard } from '@/components/movie-card/movie-card.component';
import { CardFooter } from '@/components/movie-card/card-footer/card-footer.component';
import { ImageWithFallback } from '@common/image-with-fallback/image-with-fallback.component';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { dummyData } from '@/utils/test-utils/dummy-data';
import { mockStore } from '@/utils/test-utils/mock-store';

describe('<MovieCard />', () => {
  const dummyProps = { ...dummyData[0] };
  const store = mockStore({});
  let wrapper: ReactWrapper;
  beforeEach(() => {
    wrapper = renderWithProviders(<MovieCard movie={dummyProps} />, store);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('passes correct posterPath prop', () => {
    expect(wrapper.find(ImageWithFallback).prop('src')).toMatch(dummyProps.posterPath);
  });
  it('passes correct title prop', () => {
    expect(wrapper.find(CardFooter).prop('title')).toEqual(dummyProps.title);
  });
  it('passes correct voteAverage prop', () => {
    expect(wrapper.find(CardFooter).prop('voteAverage')).toEqual(dummyProps.voteAverage);
  });
  it('passes correct releaseDate prop', () => {
    expect(wrapper.find(CardFooter).prop('releaseDate')).toEqual(dummyProps.releaseDate);
  });
  it('passes correct genres prop', () => {
    expect(wrapper.find(CardFooter).prop('genres')).toEqual(dummyProps.genres);
  });
});
