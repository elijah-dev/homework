import React, { memo, useCallback } from 'react';
import { ImageWithFallback } from '@common/image-with-fallback/image-with-fallback.component';
import { MovieModel } from '@/model/movie.types';
import { CardFooter } from '@/components/movie-card/card-footer/card-footer.component';
import { StyledMovieCard } from '@/components/movie-card/movie-card.style';
import { useDispatch } from 'react-redux';
import { setSelectedMovie } from '@/store/actions/movie.actions';
import { Link } from 'react-router-dom';
import { Path } from '@/model/enums';

export interface MovieCardProps {
  movie: MovieModel;
}

function MovieCardComponent({ movie }: MovieCardProps) {
  const { id, posterPath, title, voteAverage, releaseDate, genres } = movie;

  const dispatch = useDispatch();
  const handleClick = useCallback(() => {
    dispatch(setSelectedMovie(movie));
  }, [dispatch, movie]);

  return (
    <Link to={`${Path.Movie}/${id}`}>
      <StyledMovieCard onClick={handleClick}>
        <ImageWithFallback src={posterPath} borderRadius="10px" fitWidth />
        <CardFooter title={title} voteAverage={voteAverage} releaseDate={releaseDate} genres={genres} />
      </StyledMovieCard>
    </Link>
  );
}

export const MovieCard = memo(MovieCardComponent);
