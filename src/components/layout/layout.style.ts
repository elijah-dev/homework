import styled from 'styled-components';

export const StyledMain = styled.main`
  flex-grow: 1;
  margin-top: ${(props) => props.theme.headerHeight};
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.primary[400]};
  color: ${(props) => props.theme.colors.primary[300]};
`;
