import React from 'react';
import 'jest-styled-components';
import { Layout } from '@/components/layout/layout.component';
import { ShallowWrapper, shallow } from 'enzyme';

describe('<Layout />', () => {
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(
      <Layout>
        <div id="test" />
      </Layout>,
    );
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('renders children', () => {
    expect(wrapper.exists('#test')).toBe(true);
  });
});
