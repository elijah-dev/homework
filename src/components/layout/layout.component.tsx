import React, { memo, ReactNode } from 'react';
import { Footer } from '@/components/footer/footer.component';
import { Header } from '@/components/header/header.component';
import { StyledMain } from '@/components/layout/layout.style';

function LayoutComponent({ children }: { children: ReactNode }) {
  return (
    <>
      <Header />
      <StyledMain>{children}</StyledMain>
      <Footer />
    </>
  );
}

export const Layout = memo(LayoutComponent);
