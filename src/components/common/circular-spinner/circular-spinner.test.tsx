import React from 'react';
import { CircularSpinner } from '@common/circular-spinner/circular-spinner.component';
import { StyledCircularSpinner } from '@/components/common/circular-spinner/circular-spinner.style';
import { shallow, ShallowWrapper } from 'enzyme';

describe('<CircularSpinner />', () => {
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(
      <CircularSpinner isLoading={false}>
        <div id="test-child" />
      </CircularSpinner>,
    );
  });
  it('renders spinner correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('expect to render children when not loading', () => {
    expect(wrapper.exists('#test-child')).toBe(true);
  });
  it('expect to not render spinner when not loading', () => {
    expect(wrapper.exists('#test-child')).toBe(true);
  });
  it('expect to render spinner when loading', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.exists(StyledCircularSpinner)).toBe(true);
  });
  it('expect to not render children when loading', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.exists('#test-child')).toBe(false);
  });
});
