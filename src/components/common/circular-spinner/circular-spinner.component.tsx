import React, { memo } from 'react';
import { StyledCircularSpinner } from '@/components/common/circular-spinner/circular-spinner.style';

interface CircularSpinnerProps {
  isLoading: boolean;
  children: React.ReactNode;
}

function CircularSpinnerComponent({ isLoading, children }: CircularSpinnerProps) {
  return (
    <>
      {isLoading ? (
        <StyledCircularSpinner>
          <div />
        </StyledCircularSpinner>
      ) : (
        children
      )}
    </>
  );
}

export const CircularSpinner = memo(CircularSpinnerComponent);
