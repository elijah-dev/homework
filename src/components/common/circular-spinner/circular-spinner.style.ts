import styled from 'styled-components';

export const StyledCircularSpinner = styled.div`
  flex-grow: 1;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  & div {
    width: 60px;
    height: 60px;
    border: 8px solid ${(props) => props.theme.colors.secondary[300]};
    border-right: 8px solid rgba(0, 0, 0, 0);
    border-radius: 100%;
    animation: rotate 1.4s ease-out infinite;
  }

  @keyframes rotate {
    from {
      transform: rotate(0deg);
    }

    to {
      transform: rotate(360deg);
    }
  }
`;
