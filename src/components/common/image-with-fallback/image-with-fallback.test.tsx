import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { ImageWithFallback } from '@common/image-with-fallback/image-with-fallback.component';
import { StyledImageWithFallback } from '@common/image-with-fallback/image-with-fallback.style';
import 'jest-styled-components';

describe('<ImageWithFallback />', () => {
  let wrapper: ReactWrapper;
  beforeEach(() => {
    wrapper = mount(<ImageWithFallback />);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('passes isLoaded prop with correct value by default', () => {
    expect(wrapper.find(StyledImageWithFallback).prop('isLoaded')).toBe(false);
  });
  it('changes isLoaded prop when image loads', () => {
    wrapper.find('img').simulate('load');
    expect(wrapper.find(StyledImageWithFallback).prop('isLoaded')).toBe(true);
  });
  it('does not render fallback text by default', () => {
    expect(wrapper.exists('h3')).toBe(false);
  });
  it('renders fallback text when image throws an error', () => {
    wrapper.find('img').simulate('error');
    expect(wrapper.exists('h3')).toBe(true);
  });
});
