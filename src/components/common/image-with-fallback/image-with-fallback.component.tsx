import React, { useState, useCallback, memo, useEffect } from 'react';
import { StyledImageWithFallback } from '@common/image-with-fallback/image-with-fallback.style';

interface ImageWithFallbackProps {
  fitWidth?: boolean;
  fitHeight?: boolean;
  borderRadius?: string;
  src?: string;
  alt?: string;
}

function ImageWithFallbackComponent({
  fitWidth = false,
  fitHeight = false,
  borderRadius = '',
  src = '',
  alt = '',
}: ImageWithFallbackProps) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isError, setIsError] = useState(false);
  const [source, setSource] = useState('');

  const onImageLoad = useCallback(() => {
    setIsLoaded(true);
  }, []);

  const onImageError = useCallback(() => {
    setIsError(true);
  }, []);

  useEffect(() => {
    setSource(src);
  }, [src]);

  return (
    <StyledImageWithFallback fitWidth={fitWidth} fitHeight={fitHeight} borderRadius={borderRadius} isLoaded={isLoaded}>
      {isError ? <h3>Image not found</h3> : <img src={source} alt={alt} onLoad={onImageLoad} onError={onImageError} />}
    </StyledImageWithFallback>
  );
}

export const ImageWithFallback = memo(ImageWithFallbackComponent);
