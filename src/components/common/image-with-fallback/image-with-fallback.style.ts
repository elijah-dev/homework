import styled from 'styled-components';

interface ImgProps {
  fitWidth: boolean,
  fitHeight: boolean,
  isLoaded: boolean,
  borderRadius: string
}

export const StyledImageWithFallback = styled.div<ImgProps>`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  & img {
    width: ${(props) => (props.fitWidth ? '100%' : 'auto')};
    height: ${(props) => (props.fitHeight ? '100%' : 'auto')};
    opacity: ${(props) => (props.isLoaded ? '1' : '0')};
    border-radius: ${(props) => props.borderRadius};
  }
`;
