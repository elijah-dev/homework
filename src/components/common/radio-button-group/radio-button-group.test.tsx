import React from 'react';
import { shallow } from 'enzyme';
import { RadioButtonGroup } from '@common/radio-button-group/radio-button-group.component';

describe('<RadioButtonGroup />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <RadioButtonGroup desc="descripton">
        <div id="test-child" />
      </RadioButtonGroup>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
