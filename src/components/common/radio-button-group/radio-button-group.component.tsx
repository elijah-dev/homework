import React, { memo } from 'react';
import { StyledRadioButtonGroup } from '@common/radio-button-group/radio-button-group.style';

interface RadioButtonGroupProps {
  desc: string,
  children: React.ReactNode
}

function RadioButtonGroupComponent({ desc, children }: RadioButtonGroupProps) {
  return (
    <StyledRadioButtonGroup>
      <span>{desc}</span>
      {children}
    </StyledRadioButtonGroup>
  );
}

export const RadioButtonGroup = memo(RadioButtonGroupComponent);
