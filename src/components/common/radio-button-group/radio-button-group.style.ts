import styled from 'styled-components';

export const StyledRadioButtonGroup = styled.div`
  & span {
    color: ${(props) => props.theme.colors.primary[300]};
    padding-right: 9px;
    font-weight: 800;
  }

  & input {
    appearance: none;
  }

  & label {
    font-weight: 600;
    padding-right: 9px;
    margin-right: 6px;
    border-right: 3px solid ${(props) => props.theme.colors.secondary[400]};
    color: ${(props) => props.theme.colors.primary[200]};
  }

  & label:hover {
    cursor: pointer;
  }

  & input:checked + label {
    color: ${(props) => props.theme.colors.secondary[300]};
  }

  & input:checked + label:hover {
    cursor: default;
  }

  & label:last-child {
    margin: 0;
    padding: 0;
    border: none;
  }
`;
