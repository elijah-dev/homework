import React from 'react';
import { StyledGlobalErrorBoundary } from '@common/global-error-boundary/global-error-boundary.style';

interface GlobalErrorBoundaryState {
  hasError: boolean;
}

interface GlobalErrorBoundaryProps {
  children: React.ReactNode;
}

export class GlobalErrorBoundary extends React.Component<GlobalErrorBoundaryProps, GlobalErrorBoundaryState> {
  public readonly state: GlobalErrorBoundaryState = { hasError: false };

  static getDerivedStateFromError(): { hasError: boolean } {
    return { hasError: true };
  }

  public render(): React.ReactNode {
    const { hasError } = this.state;
    const { children } = this.props;

    if (hasError) {
      return (
        <StyledGlobalErrorBoundary>
          <h1>Something went really wrong...</h1>
          <p>Everything is broken, you can try again</p>
          <a href="/">Go back</a>
        </StyledGlobalErrorBoundary>
      );
    }
    return children;
  }
}
