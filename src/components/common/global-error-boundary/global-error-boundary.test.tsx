import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { GlobalErrorBoundary } from '@/components/common/global-error-boundary/global-error-boundary.component';
import { StyledGlobalErrorBoundary } from '@/components/common/global-error-boundary/global-error-boundary.style';

describe('<GlobalErrorBoundary />', () => {
  const TestChild = () => <div id="test-child" />;
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(
      <GlobalErrorBoundary>
        <TestChild />
      </GlobalErrorBoundary>,
    );
  });
  it('renders correctly by default', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('renders children by default', () => {
    expect(wrapper.exists(TestChild)).toBe(true);
  });
  it('does not render falback ui by default', () => {
    expect(wrapper.exists(StyledGlobalErrorBoundary)).toBe(false);
  });
  it('does not render children when error is thrown', () => {
    wrapper.find(TestChild).simulateError('error');
    expect(wrapper.exists(TestChild)).toBe(false);
  });
  it('renders fallback ui when error is thrown', () => {
    wrapper.find(TestChild).simulateError('error');
    expect(wrapper.exists(StyledGlobalErrorBoundary)).toBe(true);
  });
});
