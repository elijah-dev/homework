import styled from 'styled-components';

export const StyledGlobalErrorBoundary = styled.div`
  margin-top: 20px;
  text-align: center;

  & h1:first-child,
  p:first-child {
    color: #000000;
    margin-bottom: 10px;
  }

  & a:first-child {
    text-decoration: none;
    background-color: #303030;
    padding: 4px 8px;
    border-radius: 4px;
  }
`;
