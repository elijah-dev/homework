import React, { memo } from 'react';

interface RadioButtonProps {
  name: string;
  id: string;
  value: string;
  checkedValue?: string;
  label: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

function RadioButtonComponent({
  name,
  id,
  value,
  checkedValue,
  label,
  onChange,
}: RadioButtonProps) {
  return (
    <>
      <input
        type="radio"
        name={name}
        id={id}
        value={value}
        onChange={onChange}
        checked={checkedValue === value}
      />
      <label htmlFor={id}>{label}</label>
    </>
  );
}

export const RadioButton = memo(RadioButtonComponent);
