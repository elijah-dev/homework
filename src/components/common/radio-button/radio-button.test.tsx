import React from 'react';
import { shallow } from 'enzyme';
import { RadioButton } from '@common/radio-button/radio-button.component';

describe('<RadioButton />', () => {
  const changeHandler = jest.fn();
  const wrapper = shallow(<RadioButton name="" id="" value="" label="" onChange={changeHandler} />);
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('executes change handler recieved from props', () => {
    wrapper.find('input').simulate('change');
    expect(changeHandler).toBeCalledTimes(1);
  });
  it('unchecked by default', () => {
    expect(wrapper.find('input').prop('checked')).toBe(false);
  });
  it('unchecked by default', () => {
    wrapper.setProps({ value: 'test-value', checkedValue: 'test-value' });
    expect(wrapper.find('input').prop('checked')).toBe(true);
  });
});
