import styled from 'styled-components';

export const StyledCloseButton = styled.button`
  border: none;
  border-radius: 4px;
  padding: 3px 16px;
  background-color: ${(props) => props.theme.colors.secondary[500]};
  color: ${(props) => props.theme.colors.primary[200]};
  font-weight: 600;
  font-size: 1rem;
  text-transform: capitalize;
  position: absolute;
  bottom: 20px;
  right: 20px;

  &:hover {
    cursor: pointer;
  }

  &:focus {
    border: none;
    outline: none;
  }

  &:active {
    transform: scaleX(0.98);
  }
`;
