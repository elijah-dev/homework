import React from 'react';
import { CloseButton } from '@/components/close-button/close-button.component';
import { ReactWrapper } from 'enzyme';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import 'jest-styled-components';
import { mockStore } from '@/utils/test-utils/mock-store';

describe('<CloseButton />', () => {
  let wrapper: ReactWrapper;
  const store = mockStore({ currentSearch: { query: 'test' } });
  beforeEach(() => {
    wrapper = renderWithProviders(<CloseButton />, store);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
