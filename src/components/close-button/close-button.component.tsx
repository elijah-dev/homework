import React, { memo, useCallback } from 'react';
import { StyledCloseButton } from '@/components/close-button/close-button.style';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Path } from '@/model/enums';
import { RootState } from '@/store/reducers/root.reducer';

function CloseButtonComponent() {
  const history = useHistory();
  const query = useSelector(({ currentSearch }: RootState) => currentSearch.query);

  const handleClick = useCallback(() => {
    if (query) {
      history.replace(`${Path.Search}${query}`);
    } else history.push('/');
  }, [history, query]);

  return <StyledCloseButton onClick={handleClick}>Close</StyledCloseButton>;
}

export const CloseButton = memo(CloseButtonComponent);
