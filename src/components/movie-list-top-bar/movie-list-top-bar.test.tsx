import React from 'react';
import { ReactWrapper } from 'enzyme';
import { MovieListTopBar } from '@/components/movie-list-top-bar/movie-list-top-bar.component';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { mockStore } from '@/utils/test-utils/mock-store';

describe('<MovieListTopBar />', () => {
  let wrapper: ReactWrapper;
  const store = mockStore({ queryParams: { searchOption: '', searchValue: '', sortMethod: 'date' } });
  beforeEach(() => {
    wrapper = renderWithProviders(<MovieListTopBar moviesCount={5} />, store);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('renders correct found movie count', () => {
    expect(wrapper.text()).toMatch('5 movies found');
  });
});
