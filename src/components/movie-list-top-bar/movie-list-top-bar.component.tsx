import React, { useMemo, useState, ChangeEvent, memo, useCallback } from 'react';
import { RadioButtonGroup } from '@common/radio-button-group/radio-button-group.component';
import { RadioButton } from '@common/radio-button/radio-button.component';
import { useHistory, useLocation } from 'react-router-dom';
import { Path, SortBy } from '@/model/enums';

interface MovieListTopBarProps {
  moviesCount: number;
}

function MovieListTopBarComponent({ moviesCount }: MovieListTopBarProps) {
  const history = useHistory();
  const location = useLocation();
  const query = useMemo(() => new URLSearchParams(location.search), [location]);
  const [sortMethod, setSortMethod] = useState(query.get('sortBy') || SortBy.Date);

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSortMethod(event.target.value);
      query.set('sortBy', event.target.value);
      history.push(`${Path.Search}?${query.toString()}`);
    },
    [query, history],
  );

  return (
    <div>
      <span>{`${moviesCount} movies found`}</span>
      <RadioButtonGroup desc="Sort by">
        <RadioButton
          name="sort-options"
          id="sort-options:date"
          value={SortBy.Date}
          label="release date"
          onChange={handleChange}
          checkedValue={sortMethod}
        />
        <RadioButton
          name="sort-options"
          id="sort-options:rating"
          value={SortBy.Rating}
          label="rating"
          onChange={handleChange}
          checkedValue={sortMethod}
        />
      </RadioButtonGroup>
    </div>
  );
}

export const MovieListTopBar = memo(MovieListTopBarComponent);
