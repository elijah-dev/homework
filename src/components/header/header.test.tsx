import React from 'react';
import { shallow } from 'enzyme';
import { Header } from '@/components/header/header.component';

describe('<Header />', () => {
  const wrapper = shallow(<Header />);
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
