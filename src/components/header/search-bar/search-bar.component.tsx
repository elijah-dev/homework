import React, { memo, useCallback, useState } from 'react';
import { RadioButtonGroup } from '@common/radio-button-group/radio-button-group.component';
import { RadioButton } from '@common/radio-button/radio-button.component';
import { StyledSearchBar } from '@/components/header/search-bar/search-bar.style';
import { SearchField } from '@/components/header/search-bar/search-field/search-field.component';
import { SubmitButton } from '@/components/header/search-bar/submit-button/submit-button.component';
import { makeQuery } from '@/utils/url-functions.util';
import { useHistory } from 'react-router-dom';
import { Path, SearchBy, SortBy } from '@/model/enums';

function SearchBarComponent() {
  const history = useHistory();
  const [searchOption, setSearchOption] = useState(SearchBy.Title);
  const [searchValue, setSearchValue] = useState('');

  const handleSearchFieldChange = useCallback((event) => {
    setSearchValue(event.target.value);
  }, []);

  const handleRadioChange = useCallback((event) => {
    setSearchOption(event.target.value);
  }, []);

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      const queryString = makeQuery(searchOption, searchValue, SortBy.Date);
      history.push(`${Path.Search}${queryString}`, 'test');
      setSearchValue('');
      setSearchOption(SearchBy.Title);
    },
    [searchOption, searchValue, history],
  );

  return (
    <StyledSearchBar onSubmit={handleSubmit}>
      <div>
        <SearchField value={searchValue} onChange={handleSearchFieldChange} />
      </div>
      <div>
        <RadioButtonGroup desc="Search by">
          <RadioButton
            name="search-options"
            id="search-options:title"
            value={SearchBy.Title}
            label="title"
            onChange={handleRadioChange}
            checkedValue={searchOption}
          />
          <RadioButton
            name="search-options"
            id="search-options:genres"
            value={SearchBy.Genres}
            label="genres"
            onChange={handleRadioChange}
            checkedValue={searchOption}
          />
        </RadioButtonGroup>
        <SubmitButton />
      </div>
    </StyledSearchBar>
  );
}

export const SearchBar = memo(SearchBarComponent);
