import React from 'react';
import { SearchField } from '@/components/header/search-bar/search-field/search-field.component';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { mockStore } from '@/utils/test-utils/mock-store';

describe('<SearchField />', () => {
  const changeHandler = jest.fn();
  const store = mockStore();
  const wrapper = renderWithProviders(<SearchField value="test value" onChange={changeHandler} />, store);
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('recieves correct value prop', () => {
    expect(wrapper.find('input').first().prop('value')).toMatch('test value');
  });
  it('executes change handler recieved from props', () => {
    wrapper.find('input').simulate('change');
    expect(changeHandler).toBeCalledTimes(1);
  });
});
