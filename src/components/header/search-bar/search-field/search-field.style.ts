import styled from 'styled-components';

export const StyledSearchField = styled.input`
  width: 100%;
  background-color: ${(props) => props.theme.colors.primary[300]};
  border: none;
  border-radius: 4px;
  font-size: 1.1rem;
  font-weight: 600;
  color: ${(props) => props.theme.colors.primary[400]};
  padding: 4px;

  &:focus {
    border: none;
    box-shadow: none;
    outline: none;
    caret-color: ${(props) => props.theme.colors.primary[400]};
    background-color: ${(props) => props.theme.colors.primary[200]};
  }
`;
