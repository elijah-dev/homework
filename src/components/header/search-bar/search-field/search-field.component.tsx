import React, { memo } from 'react';
import { StyledSearchField } from '@/components/header/search-bar/search-field/search-field.style';

interface SearchFieldProps {
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

function SearchFieldComponent({ value, onChange }: SearchFieldProps) {
  return <StyledSearchField type="text" value={value} placeholder="Find movies" onChange={onChange} />;
}

export const SearchField = memo(SearchFieldComponent);
