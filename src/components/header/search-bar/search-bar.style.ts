import styled from 'styled-components';

export const StyledSearchBar = styled.form`
  min-width: 600px;

  & > div:first-child {
    margin-bottom: 10px;
  }

  & div + div {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
