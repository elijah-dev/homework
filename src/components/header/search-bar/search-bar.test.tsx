import React from 'react';
import { ReactWrapper } from 'enzyme';
import { SearchBar } from '@/components/header/search-bar/search-bar.component';
import { RadioButton } from '@common/radio-button/radio-button.component';
import { renderWithProviders } from '@/utils/test-utils/render-with-providers.util';
import { mockStore } from '@/utils/test-utils/mock-store';

describe('<SeacrhBar />', () => {
  let wrapper: ReactWrapper;
  beforeEach(() => {
    wrapper = renderWithProviders(<SearchBar />, mockStore({}));
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('changes searchValue on input', () => {
    const searchField = 'input[type="text"]';
    wrapper.find(searchField).simulate('change', { target: { value: 'testvalue' } });
    expect(wrapper.find(searchField).prop('value')).toMatch('testvalue');
  });

  it('has one radio button checked by default', () => {
    const checkedBtns = wrapper.find(RadioButton).filterWhere((btn) => btn.prop('value') === btn.prop('checkedValue'));
    expect(checkedBtns.length).toBe(1);
  });
  it('changes checkedValue of a radio button to its own value on change', () => {
    const uncheckedBtn = wrapper
      .find(RadioButton)
      .findWhere((btn) => btn.prop('value') !== btn.prop('checkedValue'))
      .first();
    const uncheckedBtnValue = uncheckedBtn.prop('value');
    expect(wrapper.find(`input[value="${uncheckedBtnValue}"]`).parent().prop('checkedValue')).toMatch(
      uncheckedBtnValue,
    );
  });
});
