import React, { memo } from 'react';
import { StyledSubmitButton } from '@/components/header/search-bar/submit-button/submit-button.style';

function SubmitButtonComponent() {
  return <StyledSubmitButton type="submit" value="find" />;
}

export const SubmitButton = memo(SubmitButtonComponent);
