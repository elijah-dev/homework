import styled from 'styled-components';

export const StyledSubmitButton = styled.input`
  border: none;
  border-radius: 4px;
  padding: 3px 16px;
  background-color: ${(props) => props.theme.colors.secondary[500]};
  color: ${(props) => props.theme.colors.primary[200]};
  font-weight: 600;
  font-size: 1rem;
  text-transform: capitalize;

  &:hover {
    cursor: pointer;
  }

  &:focus {
    border: none;
    outline: none;
  }

  &:active {
    transform: scaleX(0.98);
  }
`;
