import React, { memo } from 'react';
import { SearchBar } from '@/components/header/search-bar/search-bar.component';
import { StyledHeader } from '@/components/header/header.style';
import { Link } from 'react-router-dom';

function HeaderComponent() {
  return (
    <StyledHeader>
      <h1>
        <Link to="/">
          <span>movie</span>
          <span>finder</span>
        </Link>
      </h1>
      <SearchBar />
    </StyledHeader>
  );
}

export const Header = memo(HeaderComponent);
