import styled from 'styled-components';

export const StyledHeader = styled.header`
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 2;
  background-color: ${(props) => props.theme.colors.primary[600]};
  height: ${(props) => props.theme.headerHeight};
  flex-shrink: 0;
  padding: 10px;
  padding-right: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  & a {
    color: ${(props) => props.theme.colors.primary[100]};
    font-size: 3.5rem;
    text-transform: uppercase;
    font-weight: 600;
    text-decoration: none;
  }

  & a span + span {
    color: ${(props) => props.theme.colors.secondary[400]};
  }

`;
