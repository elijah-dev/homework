const path = require('path');

const outputPath = path.resolve(process.cwd(), 'dist');

module.exports = {
  mode: process.env.NODE_ENV,
  output: {
    filename: '[name].js',
    path: outputPath,
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    alias: {
      '@': path.resolve(__dirname, '../src/'),
      '@common': path.resolve(__dirname, '../src/components/common/'),
      'test-utils': path.resolve(__dirname, '../tests/test-utils/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|ts)x?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|png|svg|jpg|gif|ico)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
    ],
  },
};
