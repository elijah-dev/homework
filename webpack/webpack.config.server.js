const webpack = require('webpack');
const { merge } = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const common = require('./webpack.config.common');

module.exports = merge(common, {
  name: 'server',
  target: 'node',
  entry: './src/server-renderer.tsx',
  externals: [nodeExternals()],
  output: {
    filename: 'server-renderer.js',
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        include: /src/,
        use: [
          'css-loader',
        ],
      },
    ],
  },
  plugins: [
  ],
});
