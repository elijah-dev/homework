module.exports = {
  clearMocks: true,
  collectCoverageFrom: ['**/src/**'],
  coveragePathIgnorePatterns: ['/node_modules/', '(.*)\.snap$'],
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[tj]s?(x)'],
  testPathIgnorePatterns: ['/node_modules/'],
  moduleNameMapper: {
    '^@/(.*)': '<rootDir>/src/$1',
    '^@common/(.*)': '<rootDir>/src/components/common/$1',
    '^test-utils/(.*)': '<rootDir>/tests/test-utils/$1',
    '\\.(css|less)$': '<rootDir>/src/utils/test-utils/mock-style.js'
  },
  snapshotSerializers: ['enzyme-to-json/serializer'],
  snapshotResolver:  './jest.snapshot-resolver.js',
};
